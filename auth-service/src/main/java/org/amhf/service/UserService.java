package org.amhf.service;

import org.amhf.domain.User;

public interface UserService {
	void create(User user);
}
